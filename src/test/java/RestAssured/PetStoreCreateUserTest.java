package RestAssured;

import RestAssured.DTO.User;
import RestAssured.DTO.UserOut;
import RestAssured.Service.UserApi;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

public class PetStoreCreateUserTest {

    @Test
    public void createUserTest(){
        UserApi userApi = new UserApi();

        User user = new User(1,
                "UserName",
                "FirstName",
                "LastName",
                "email",
                "password",
                "phone",
                2);
        Response response = userApi.createUser(user);

        //1 вариант проверок
        response
                .then()
                .log().all()
                .statusCode(200)
                .time(lessThan(5000L))
                .body("code", equalTo(200))
                .body("type", equalTo("unknown"))
                .body("message", equalTo("1"));

        //2 вариант
        int actualCode = response.jsonPath().get("code");
        Assertions.assertEquals(200, actualCode);

        //3 вариант
        UserOut userOut = response.then().extract().body().as(UserOut.class);
        Assertions.assertEquals(200, userOut.getCode());
        Assertions.assertEquals("1", userOut.getMessage());

    }
}
