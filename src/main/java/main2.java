import Animals.*;
import org.w3c.dom.css.CSS2Properties;

public class main2 {
    public static void main(String[] args){
        Dog dog = new Dog("Жучка", 13,4);
        dog.happyBirthDay();
        System.out.println(dog.getAge());
        dog.Say();

        Cat cat = new Cat("Барсик", 1);
        System.out.println(cat.getAge());
        cat.Say();

       // Animal animal = new Animal("Имя", 12);

        Flying duck = new Duck("Утка",2);
        Flying bug = new Bug();

        duck.fly();
        bug.fly();

        Object duck2 = new Duck("Селезень", 2);

        Animal[] animals = new Animal[2];
        animals[0] = new Cat("Фунтик", 3);
        animals[1] = new Dog("Тузик",5);
        for (Animal a : animals){
            System.out.println(a.toString());
            System.out.println(a.hashCode());
        }
        Cat cat1 = (Cat) animals[0];
        ((Cat)animals[0]).Say();

        System.out.println(cat1.equals(new Dog("",2)));
        System.out.println(cat1.equals(new Cat("Фунтик",3)));
    }
}
