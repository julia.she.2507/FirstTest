package Animals;

public class Cat extends Animal{

    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public void log() {
        System.out.println("Лог Кошки");
    }

    @Override
    public void Say(){
        System.out.println("Мяу!");
    }


    @Override
    public int hashCode() {
        return super.hashCode()*100+this.getAge();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cat)) return false;
        Cat equalsCat = (Cat) obj;
        return name.equals(equalsCat.name) &&
                age == equalsCat.age;
    }


    @Override
    public String toString() {
        return "Cat{name = " + this.getName() + ", age = " + this.getAge() + "}";
    }
}
