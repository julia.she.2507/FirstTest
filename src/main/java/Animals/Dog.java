package Animals;

public class Dog extends Animal{


    private int countleg;

    public Dog(String name, int age, int countleg) {
        super(name, age);
        this.countleg = countleg;
    }

    public Dog(String name, int age) {
        super(name, age);
        this.countleg = 4;
    }

    @Override
    public void log() {

    }


    public int getCountleg() {
        return countleg;
    }

    public void setCountleg(int countleg) {
        this.countleg = countleg;
    }
    @Override
    public void Say(){
        System.out.println("Гав!");
    }
}
