package Animals;

public abstract class Animal {
    public String name;
    public int age;

    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }
    public abstract void log();

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Animal setAge(int age) {
      //  if (age<0) {this.age = 0;}
       // else {this.age = age;}
        return this;
    }

    public void happyBirthDay(){
        age++;
    }


    public void Say(){
        System.out.println("Привет! я объект класса Animal");
    }


}
