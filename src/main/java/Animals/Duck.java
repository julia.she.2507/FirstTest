package Animals;

public class Duck extends Animal implements Flying{
    public Duck(String name, int age) {
        super(name, age);
    }

    @Override
    public void log() {

    }

    @Override
    public void fly() {
        System.out.println("Я лечу");
    }
}
