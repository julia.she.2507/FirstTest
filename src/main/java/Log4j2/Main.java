package Log4j2;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class); // объявили переменную класса для лога

    public static void main(String[] args) {
        /*
        logger.info("INFO"); // вызов логирования уровня инфо
        logger.warn("WARN"); // вызываем в тех местах где требуется
        logger.error("ERROR"); // error и fatal нужно всегда логировать
        logger.trace("TRACE");
        logger.fatal("FATAL");
*/
        int[] i = new int[2];
        try {
            i[4] = 3;
        }catch (ArrayIndexOutOfBoundsException e){
            StackTraceElement[] st = e.getStackTrace();
            for(StackTraceElement s : st){
                logger.fatal("Вышли за границы " + s.toString());
            }
        } catch (Exception e){
            StackTraceElement[] st = e.getStackTrace();
            for(StackTraceElement s : st){
                logger.fatal("Непредвиденная ошибка " + s.toString());
            }
        }

     }
}

