package RestAssured.Service;
import RestAssured.DTO.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class UserApi {
    private final String BASE_URL = "https://petstore.swagger.io/v2";
    private final String USER = "/user";

    public Response createUser(User user){
        return given()
                .baseUri(BASE_URL)
                .contentType(ContentType.JSON)
                .with()
                .body(user)
                .log().all()
                .when()
                .post(USER);
    }
}