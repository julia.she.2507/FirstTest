package RestAssured.DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/*
{
  "code": 200,
  "type": "unknown",
  "message": "6874986966"
}

 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@JsonSerialize
public class UserOut {
    private int code;
    private String type;
    private String message;
}
