package RestAssured.DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/*
{
  "id": 0,
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "password": "string",
  "phone": "string",
  "userStatus": 0
}
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
// используется в body
public class User {
    private int id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private int userStatus;

}
