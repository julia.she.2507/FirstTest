package lombok;

import lombok.experimental.FieldDefaults;

@Data
@Builder(toBuilder = true)
@FieldDefaults(makeFinal=false, level=AccessLevel.PRIVATE)
public class Man {
    @NonNull
    String name;

    String company;

    @NonNull
    @Builder.Default
    int age = 0;

}

