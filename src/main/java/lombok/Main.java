package lombok;

public class Main {
    public static void main(String[] args) {
        User user = new User("Julia", 18);
        System.out.println(user.getAge());
        System.out.println(user.getName());
        user.setAge(22);
        user.setName("Ivan");
        System.out.println(user.toString());
        user.logMe("Log");


        Car car = new Car("Model", "Gos. Number", "Brand");
        System.out.println(car.getBrand());
        System.out.println(car.getModel());
        System.out.println(car.getNumber());
        System.out.println(car.toString());

        Man man = Man.builder()
                .name("Petya")
                .company("Company")
                .build();
        System.out.println(man.getName());
        System.out.println(man.getAge());

        Man man2 = new Man("", "", 12);

    }
}
