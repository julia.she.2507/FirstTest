package lombok;
import lombok.extern.log4j.Log4j2;
@Data
@Log4j2
public class User {
    @NonNull
    private String name;
    @NonNull
    private int age;

    public void logMe(String log){
        User.log.info(log);
    }
}

