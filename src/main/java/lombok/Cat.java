package lombok;
@Data
public class Cat {
    @NonNull
    private String name;
    @NonNull
    private Integer age;
}
