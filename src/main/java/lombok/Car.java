package lombok;
@Value
public class Car {
    @NonNull
    private String model;

    @NonNull
    private String number;

    @NonNull
    private String brand;
}



