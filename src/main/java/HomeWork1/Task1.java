package HomeWork1;
import java.util.*;


public class Task1 {
    public static void main(String[] args){
        //объявляем массив
        float[] numbers = new float[10];
        System.out.println("Размер массива - " + numbers.length);
        //наполняем массив
        for (int i=0; i < numbers.length; i++){
            numbers[i] = Math.round(Math.random()*100);
         //   System.out.println(numbers[i]);
        }
        System.out.print("Элементы массива: ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        //определяем максимальный элемент
        float max = numbers[0];
        for (int i=1; i < numbers.length; i++){
            max = Math.max(max,numbers[i]);
            //System.out.println(numbers[i]);
        }
        System.out.println("\nМаксимальный элемент в массиве numbers: " + max);
        //определяем минимальный элемент
        float min = numbers[0];
        for (int i=1; i < numbers.length; i++){
            min = Math.min(min,numbers[i]);
            //System.out.println(numbers[i]);
        }
        System.out.println("Минимальный элемент в массиве numbers: " + min);
        //определяем минимальный элемент
        float sum = 0;
        for (int i=0; i < numbers.length; i++){
            sum = sum + numbers[i];
            //System.out.println(numbers[i]);
        }
        float avg = sum/numbers.length;
        System.out.println("Среднее арифметическое элементов в массиве numbers: " + avg);
    }
}
