package CollectionLesson;
import java.util.*;

public class Main {
    public static void main(String[] args){
/*        List<String> list1=new ArrayList<>();
  //      ArrayList<String> list2 = new ArrayList<String>(list1);
        ArrayList<String> list3 = new ArrayList<String>(30);

        list1.add("Привет");
        System.out.println(list1);
        list1.add(0,"World");
        list1.set(1,"Hello");
        System.out.println(list1);
        System.out.println(list1.isEmpty());

        String res = list1.get(1) + " " + list1.get(0);
        System.out.println(res);
        ArrayList<String> list2 = new ArrayList<String>(list1);

        list1.remove("World");
        for (String el : list1){
            System.out.println(el);
        }
        System.out.println(list2);
*/
        /*
        ArrayDeque<String> eque = new ArrayDeque<String>();
        eque.addLast("Включить компьютер");
        eque.addLast("Зайти в BIOS");
        eque.addLast("Поменять приоритет");
        System.out.println(eque);
        Integer i = 0;
        while (!eque.isEmpty()){  //до тех пор пока очередь не пуста
            String el = eque.pollFirst(); // сними первый элемент из очереди и положи его в переменную
            System.out.println(el); // выведи в консоль
            //i++;
            //eque.addLast(i.toString()); // строкове представление i пишем в конец очереди
        }
        System.out.println(eque);
*/
        /*
        HashSet<String> set = new HashSet<String>();
        set.add("Russia");
        set.add("USA");
        set.add("Russia");
        System.out.println(set);

         */
        HashMap<String,String> map = new HashMap<String, String>();
        map.put("Hello", "Привет");
        map.put("World", "Мир");
        System.out.println(map.get("Hello") + ", " + map.get("World") + "!");
        //двумерный массив нефиксированного размера
        ArrayList<ArrayList<String>> m = new ArrayList<ArrayList<String>>();
        m.add(new ArrayList<>());
        m.get(0).add("Hello");
    }
}

