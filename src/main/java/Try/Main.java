package Try;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[3];
        try {
            array[4] = 3;
        } catch (Exception ex) {
            //ex.printStackTrace(); // удобно для дебага
            System.out.println("Вы вышли за границы массива");
        }
        System.out.println("End");

        try {
            int i = sub(4, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int k = sum(8, 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static int sum(int a, int b) throws Exception {
        if ((a<0) || (a<b)) throw new Exception("Must be > 0");
        return a + b;
        }
    public static int sub(int a, int b) throws Exception{
        if ((a<0) || (a<b)) throw new Exception("Must be > 0");
        return a - b;
    }

}
