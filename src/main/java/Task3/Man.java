package Task3;

public class Man {
    public String name;
    public String surname;
    public int age;

    public Man(String name,String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<0) {this.age = 0;}
        else {this.age = age;}
        this.age = age;
    }
    public void Say(){
        System.out.println("Я говорю");
    }
    public void Go(){
        System.out.println("Я иду");
    }
    public void Drink(){
        System.out.println("Я пью");
    }
    public void Eat(){
        System.out.println("Я ем");
    }

    @Override
    public String toString() {
        String year = " лет.";
        if (this.age % 10 == 1 && this.age != 11) {year = " год.";}
        else if (this.age % 10 > 1 && this.age % 10 < 5) {year = " года.";}

        return "Привет! Меня зовут " + this.getName() + " " + this.getSurname() + ", мне " + this.getAge() + year;
    }
    //Привет! меня зовут name surname, мне age лет(/год/года)
}
